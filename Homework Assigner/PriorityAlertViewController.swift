//
////  PriorityAlertViewController.swift
////  Homework Assigner
////
////  Created by Tiffanie Birrell on 12/12/17.
////  Copyright © 2017 Tiffanie Birrell. All rights reserved.
////
//
//import UIKit
//
//class PriorityAlertViewController {
//
////    override func viewDidLoad() {
////        super.viewDidLoad()
////
////        // Do any additional setup after loading the view.
////    }
////
////    override func didReceiveMemoryWarning() {
////        super.didReceiveMemoryWarning()
////        // Dispose of any resources that can be recreated.
////    }
//
//    let alert = UIAlertController(title: "Priority Rank", message: "Rank the importance of this assignment", preferredStyle: .alert)
//
//    // Add 1 textField and customize it
//    alert.addTextField { (textField: UITextField) in
//    textField.keyboardAppearance = .dark
//    textField.keyboardType = .default
//    textField.autocorrectionType = .default
//    textField.placeholder = "Type something here"
//    textField.clearButtonMode = .whileEditing
//    }
//
//    // Submit button
//    let submitAction = UIAlertAction(title: "Submit", style: .default, handler: { (action) -> Void in
//        let textField = alert.textFields![0]
//        print(textField.text!)
//    })
//
//    // Cancel button // don't need, find way to center ^ button, or connect this to also delete assignment
//    let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//}

