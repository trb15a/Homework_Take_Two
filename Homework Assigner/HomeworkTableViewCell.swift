//
//  HomeworkTableViewCell.swift
//  HomeworkAssignment
//
//  Created by Tiffanie Birrell on 12/12/2017.
//  Copyright © 2017 Tiffanie Birrell. All rights reserved.
//

import UIKit

class HomeworkTableViewCell: UITableViewCell {
    
    //MARK: Properties
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priorityLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
